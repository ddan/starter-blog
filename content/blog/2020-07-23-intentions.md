---
title: "Intentions for this Blog"
date: 2020-07-23
tags:
  - Recovery
draft: false
---

I've been slowly unravelling, over the past few years, trying to figure out how I got to where I did. If there was a single straightforward answer to that question, then I might not need to make this blog.

<!-- excerpt -->

I don't seek to justify any of my actions, but I do recognize there are reasons beyond the surface. For example, I've always been pretty conflict avoidant. I bottled up frustrations, without confronting the person who stimulated them. That is part of a pattern of avoiding difficult emotions, that I see as close to the heart of my addictions.

Why didn't I confront my problems head on? Well, for one thing, I didn't have a positive means for doing so. A lot of what I was coming up against has its roots in retributive justice. I recognized an injustice, though didn't really understand it. All I knew is I didn't want to participate. 

I tried to get away, but had no conception of what I was running from, or even where I was trying to get to. In fact, I still possessed, at my core, the type of thinking at the root of what I was trying to evade. It's taken me too long to realize, what I was trying to escape from was embedded in my own consciousness.

I can't go back and change the past. What I can do, with the time I have left, is try and make it easier for others to avoid some of the pitfalls I've encountered. Maybe there's still an opportunity for me to leave the earth better than I found it.

<script data-goatcounter="https://restorative.goatcounter.com/count" async src="//gc.zgo.at/count.js"></script>
