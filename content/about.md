---
title: About
date: 2020-07-23
menu: main
---

I quit drinking and drugs almost three years ago, although I've only been approaching something resembling sobriety since the beginning of this year. 

Before I quit drinking, I'd been on a downward spiral for quite some time. My behavior and mindset deteriorated to a shameful state. I was stuck in a cycle of addiction that I didn't know how to escape. When I drank, I didn't have to be concerned with anything of import, I'd regulate my emotions with a 6 pack. I knew I had to find a different way, but that was easier said than done.

Gratefully, I've had some help coming to the stark realization of exactly how far I'd fallen.

![](/images/david.jpeg)

I grew up with a lot of pain that I attributed to the world around me. While I had some just grievances, much of my angst was misguided. It's taken me too long to realize that. Instead I bitterly rejected what I saw as adversarial social convention meant to control. I inherently distrusted all social convention, which was a direct impediment to becoming a part of something larger than myself.

I set out, in 2007, as a seeker of truth. I had the idea that the world was broken. That corruption and lies were embedded into every sphere of life, that the system was set up against the people for the benefit of the few. 

To whatever extent that may have been right, my reaction to those perceived injustices was the primary source for my suffering.

While I shed the external trappings of domination, I sought fulfillment in drugs, alcohol, sex, self-serving pursuits and fleeting experiences. I was full of bitterness, on a self-indulgent trip simply trying to escape, doing nothing to improve my situation or the world around me. 

Meanwhile, plenty of others have been working tirelessly to make the world a better place, and actually improved many of the issues I care about.. and I was falling behind.

I have a lot of remorse, and have suffered a great deal of anguish about some of my actions, and state of being, over my years on the road. I was very judgemental, had an unhealthy attitude towards women, made increasingly poor decisions, and grew to cherish my addictions over reason.

I'm here to try and speak to all of that, to open myself to communication, make clear where my current state of mind and intentions are at.

### Nonviolent Communication

A major turning point in my recovery has been finding [**Nonviolent Communication (NVC)**](https://baynvc.org/basics-of-nonviolent-communication/), at the recommendation of my counselor. NVC is now the cornerstone to my recovery, easily the most valuable teaching that I've encountered, and the main reason I'm here and able to write this message. 

I'd heard of it years ago, thought I knew what it was and even practiced it to a limited extent, but I had no inkling of its depth or potency. 

Nonviolent communication (NVC) consists of a value system that challenges ways of thinking that create violence on this planet. 

[Nonviolent Communication: A Language of Life](https://www.cnvc.org/training/resource/book-chapter-1) - Marshall Rosenberg

I'm still pretty new to the subject, but learning about the process and it's underlying philosophy immediately improved my relationship with family, has helped me to deal with anger, by recognizing where it's misplaced, and began helping me to repair old wounds. 

It's important for me to not only find healing, but make whatever positive impact in the lives of others that I'm able, and perhaps repair some of the damage I perpetuated. 

### Trying to find a path forward

Lately, I've been delving deeply into the mechanics behind addiction, Cognitive Behavioral Therapy (CBT), emotional literacy, mindfulness, restorative practices, along with important movements of our day, including Black Lives Matter and the #metoo movement.

I'm not able to devote all of my time to these pursuits, but my energy has been shifting in that direction, since around the new year. I have been saving up a lot of bookmarks, so I'd like to begin sharing regularly on *all of the above* and see where that goes.


<script data-goatcounter="https://restorative.goatcounter.com/count" async src="//gc.zgo.at/count.js"></script>
